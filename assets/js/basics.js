window.onload = () => {

//iteración 1: variables

let myFavoriteHero = "hulk", x = 50, h = 5, y = 10, z = h+y;
console.log(` 
   My favorite hero: ${myFavoriteHero}, 
   variable x: ${x}, 
   variable h: ${h}, 
   variable y: ${y}, 
   variable z: ${z}.
   
   `);



/******************************************************************** */



//Iteración 2: variables avanzadas
const character = {name: 'Jack Sparrow', age: 10};
character.age = 25;

let firstName = "John", lastName = "Snow", age = 24;
///console.log(`Soy ${firstName} ${lastName}, tengo ${age} y me gustan los lobos`);

//Suma precios
const toy1 = {name: 'Buss myYear', price: 19};
const toy2 = {name: 'Rallo mcKing', price: 29};
sumaPrecios =  (a, b) => {console.log(a+b); return (a + b)};
//sumaPrecios(toy1.price, toy2.price);


//Suma totalPrice
let globalBasePrice = 10000;
globalBasePrice = 25000;
const car1 = {name: 'BMW m&m', basePrice: 50000, finalPrice: 60000};
const car2 = {name: 'Chevrolet Corbina', basePrice: 70000, finalPrice: 80000};

const SumaBasePrice = ()=>{
    let mergecar = [car1,car2];

    for (items in mergecar) {
        mergecar[items].finalPrice = mergecar[items].basePrice + globalBasePrice;
        console.log(`El precio final es: ` + mergecar[items].finalPrice )
        }
}
//Llamada desactivada
//SumaBasePrice();




/*********************************************************************/



//Iteración #3: Operadores

const operations = (a,b,c,d,e,f) => {
alert(`la suma de ${a} + ${b} es:  ${a*b} `);
alert(`la division de ${c} / ${d} es:  ${c/d}`);
alert(`la el resto de la division de ${e} y ${f} es: ${parseInt(e/f)}`);
alert(a+=b);
alert(b*=a);
};

//operations(5,10,10,2,15,9);




/*********************************************************************/


// Iteración #4: Arrays


//Avengers
const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

let firstAvenger = avengers[0];
console.log(`
1.1 // 
la variable firstAvenger es ${firstAvenger}`);

firstAvenger = "IRON MAN";
console.log(`
1.2 // 
la variable firstAvenger ahora es ${firstAvenger}`);

//1.3
alert(`la longuitud del array Avengers es: ${avengers.length}`);



// Rick and Morty 

//1.4
const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
const newCharacters = ['Morty', 'Summer']

let addCharacters = (arr) => {
for(character in arr){
    character.push(newCharacters);
}};

const allCharacter = [...rickAndMortyCharacters, ...newCharacters]


console.log(`
1.4 // 
New Characters: ${newCharacters}
All Characters: ${allCharacter}
`);

//1.5

allCharacter.pop();

let fistCharacter = allCharacter[0];
let lastCharacter = allCharacter.slice(-1)[0]

console.log(`
//1.6
First: ${fistCharacter}
Last:  ${lastCharacter}
`)




//1.6

allCharacter.splice(1,1);

console.log(`
//1.6
${allCharacter}

`)



/*********************************************************************/


// Iteración #5: Condicionales




const number1 = 10;
const number2 = 20;
const number3 = 2;


if(number1 === 10){
    console.log('number1 es estrictamente igual a 10')
}

if ( number2/number1 === 2) {
  console.log("number2 dividido entre number1 es igual a 2");
}

if (number1 !== number2) {
  console.log("number1 es estrictamente distinto a number2");
}

if (number3 != number1) {
  console.log("number3 es distinto number1");
}

if (number3 * number1 === 5) {
  console.log("number3 por 5 es igual a number1");
}

if (number3 * number1 && number1*2 === number2) {
  console.log("number3 por 5 es igual a number1 Y number1 por 2 es igual a number2");
}

if (number2/2 === number1 || number1/5 === number3) {
  console.log("number2 entre 2 es igual a number1 O number1 entre 5 es igual a number3");
};



//Iteración #6: Bucles

//1.1
for(i=0; i <= 9; i++){
    console.log('contando: ' + i);
}

//1.2

for(i=0; i <= 9; i++){
    if(i % 2 === 0){
    console.log('el resto 0: ' + i);
}};


//1.3

for(i=10; i >= 0; i--){
    if(i === 0){
        console.log('zzz...zzz....zzz')
    }else{
    console.log('intentado dormir...' + i)}
};




};










